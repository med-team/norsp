Summary: predictor of NOn-Regular Secondary Structure
Name: norsp
Version: 1.0.2
Release: 1
License: GPL
Group: Applications/Science
URL: https://rostlab.org/owiki/index.php/NORSp_-_predictor_of_NOn-Regular_Secondary_Structure
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires:  pp-popularity-contest, profphd, profphd-utils, coiledcoils

%description
 Many structurally flexible regions play important roles in biological processes. It has been shown that extended loopy regions are very abundant in nature, and that 
 they are evolutionarily conserved. NORSp is a publicly available predictor for disordered regions in protein. Specifically, it predicts long regions
 with no regular secondary structure. Upon user submission of protein sequence, NORSp will analyse the protein about its secondary structure, and presence of 
 transmembrane helices and coiled-coil. It will then return e-mail to user about the presence and position of disordered regions.NORSp can be useful for 
 biologists in several ways. For example, crystallographers can check whether their proteins contain NORS regions and make the decision about whether to 
 proceed with the experiments since NORS proteins may be difficult to crystallise, as demonstrated by the their low occurrence in PDB. Biologists interested in protein
 structure-function relationship may also find it interesting to verify whether the protein-protein interaction sites coincide with NORS region.

%prep
%setup -q

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS
%doc COPYING
/usr/bin/*
%{_mandir}/*/*
%{_datadir}/%{name}/*

%changelog
* Tue Jun 21 2011 Laszlo Kajan <lkajan@rostlab.org - 1.0.2-1
- Initial build.
* Sun Jun 19 2011 Guy Yachdav <gyachdav@rostlab.org - 1.0.0-1
- Initial build.
