Source: norsp
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Laszlo Kajan <lkajan@rostlab.org>,
           Eva Reisinger <e.reisinger@gmx.net>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13), debhelper
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/norsp
Vcs-Git: https://salsa.debian.org/med-team/norsp.git
Homepage: https://rostlab.org/owiki/index.php/NORSp_-_predictor_of_NOn-Regular_Secondary_Structure
Rules-Requires-Root: no

Package: norsp
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Recommends: blast2,
            librg-utils-perl,
            profphd,
            ncoils
Multi-Arch: foreign
Description: predictor of non-regular secondary structure
 NORSp is a publicly available predictor for disordered regions in proteins.
 Specifically, it predicts long regions with no regular secondary structure.
 Upon submission of a protein sequence, NORSp analyses the protein about its
 secondary structure, the presence of transmembrane helices and coiled-coils.
 It then returns the presence and position of disordered regions.
 .
 NORSp can be useful for biologists in several ways. For example,
 crystallographers can check whether their proteins contain NORS regions and
 make the decision about whether to proceed with the experiments since NORS
 proteins may be difficult to crystallise, as demonstrated by the their low
 occurrence in PDB. Biologists interested in protein structure-function
 relationship may also find it interesting to verify whether the
 protein-protein interaction sites coincide with NORS regions.
